package server

import (
	chatService "fulfillment_auth/application/chat/delivery/grpc/service"
	chatUseCase "fulfillment_auth/application/chat/usecase"
	"fulfillment_auth/config"
	"fulfillment_auth/package/logger"
	pbChat "fulfillment_auth/proto/chat"
	"github.com/go-redis/redis/v8"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"net"
	userService "fulfillment_auth/application/user/delivery/grpc/service"
	userUseCase "fulfillment_auth/application/user/usecase"
	pbUser "fulfillment_auth/proto/auth"
	userRepository "fulfillment_auth/application/user/repository"
)

type Server struct {
	logger logger.ILogger
	cfg	*config.Config
	db *gorm.DB
	redisClient *redis.Client
}

func InitServer(logger logger.ILogger, cfg *config.Config, db *gorm.DB, redisClient *redis.Client) *Server {
	return &Server{logger: logger,cfg: cfg, db: db, redisClient: redisClient}
}

func (s *Server) Run() error  {
	s.logger.Info("Begin start server listen on : ",s.cfg.Server.Port)
	listen,err := net.Listen("tcp",s.cfg.Server.Port)
	if err!= nil {
		return err
	}
	serverRpc := grpc.NewServer()
	// Start Register Server Chat
	chatUserCase:= chatUseCase.NewChatUseCase(s.cfg,s.logger)
	serverHandleChat := chatService.InitChatGRPC(s.cfg,s.logger,chatUserCase)
	pbChat.RegisterChatServer(serverRpc,serverHandleChat)
	// End
	// Start Register Server User
	userSqlRepo:= userRepository.InitUserRepository(s.db)
	userRedisRepo:= userRepository.InitAuthRedisRepo(s.redisClient)
	userUC := userUseCase.InitAuthUseCase(s.cfg, s.logger,userSqlRepo,userRedisRepo)
	serverHandleUser:= userService.InitUserGRPC(s.cfg,s.logger,userUC)
	pbUser.RegisterAuthServiceServer(serverRpc,serverHandleUser)
	// End

	if err:=serverRpc.Serve(listen);err!=nil {
		return err
	}
	// End Register Server
	return nil
}