package service

import (
	"fulfillment_auth/application/chat"
	"fulfillment_auth/config"
	"fulfillment_auth/package/logger"
)
// Chat handlers
type chatService struct {
	cfg    *config.Config
	logger logger.ILogger
	chatUseCase chat.ChatUseCase
}


// NewNewsHandlers News handlers constructor
func InitChatGRPC(cfg *config.Config, logger logger.ILogger, chatUseCase chat.ChatUseCase) *chatService {
	return &chatService{cfg: cfg, logger: logger, chatUseCase: chatUseCase}
}

