package service

import (
	pb "fulfillment_auth/proto/chat"
	"context"
)

func (s *chatService) SayHello(ctx context.Context, r *pb.Message) (*pb.Message, error) {
	strFromUseCase,err:= s.chatUseCase.SayHello(ctx,r.Bodymessage)
	if err!=nil {
		s.logger.Error("Error when call %v",err)
	}
	s.logger.Info(strFromUseCase)
	// get Message form usercase

	return &pb.Message{Bodymessage: strFromUseCase +r.Bodymessage+" #"},nil
}
