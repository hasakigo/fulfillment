package usecase

import (
	"fulfillment_auth/config"
	"fulfillment_auth/package/logger"
	"context"
	"fmt"
	"time"
)

type chatUseCase struct {
	logger logger.ILogger
	cfg	*config.Config

}

func NewChatUseCase(cfg *config.Config, logger logger.ILogger) *chatUseCase  {
	return &chatUseCase{cfg: cfg,logger: logger}
}

func (c *chatUseCase) SayHello(ctx context.Context, string string ) (string, error) {
	fmt.Println("Context state", ctx)
	fmt.Println("Receive message from client with value #",ctx)
	timeFormat := time.Now().Format("2006-01-02 15:04:05")
	return "--- wao Hello service auth response is #"+string+" #" +timeFormat,nil
}

