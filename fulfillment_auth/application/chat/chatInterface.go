package chat

import (
	"context"
)

type ChatUseCase interface {
	SayHello(ctx context.Context, str string) (string, error)
}