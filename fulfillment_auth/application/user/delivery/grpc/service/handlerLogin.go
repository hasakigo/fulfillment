package service

import (
	"context"
	"fulfillment_auth/application/models"
	"fulfillment_auth/package/grpc_errors"
	"fulfillment_auth/package/utils"
	pb "fulfillment_auth/proto/auth"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
	"strconv"
)

func (u *userService) Login(ctx context.Context, request *pb.LoginRequest) (*pb.LoginResponse ,error){
	email:= request.GetEmail()
	if !utils.ValidateEmail(email) {
		u.logger.Errorf("Validate Email errors: %v",email)
		return nil, status.Errorf(codes.InvalidArgument, "ValidateEmail: %v", email)
	}
	userWithToken,err:= u.userUseCase.Login(ctx, email,request.GetPassword())
	if err != nil {
		u.logger.Errorf("userUC.Login: %v", err)
		return nil, status.Errorf(grpc_errors.ParseGRPCErrStatusCode(err), "Login: %v", err)
	}
	return &pb.LoginResponse{
		User: u.userModelToProto(userWithToken.User),
		Token: userWithToken.Token,
	}, err
}


func (u *userService) userModelToProto(user *models.User) *pb.User {
	userProto := &pb.User{
		Id:        strconv.Itoa(user.ID),
		Email:     user.Email,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Password:  user.Password,
		CreatedAt: timestamppb.New(user.CreatedAt),
		UpdatedAt: timestamppb.New(user.UpdatedAt),
	}
	return userProto
}