package service

import (
	"fulfillment_auth/application/user"
	"fulfillment_auth/config"
	"fulfillment_auth/package/logger"
)

type userService struct {
	cfg    *config.Config
	logger logger.ILogger
	userUseCase user.UserUseCase
}

func InitUserGRPC(cfg *config.Config, logger logger.ILogger, userUseCase user.UserUseCase) *userService {
	return &userService{cfg: cfg, logger: logger, userUseCase: userUseCase}
}
