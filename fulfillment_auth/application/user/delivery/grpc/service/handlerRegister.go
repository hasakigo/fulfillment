package service

import (
	"context"
	"fulfillment_auth/application/models"
	"fulfillment_auth/package/grpc_errors"
	"fulfillment_auth/package/utils"
	pb "fulfillment_auth/proto/auth"
	"google.golang.org/grpc/status"
)

func (u *userService) Register(ctx context.Context, request *pb.RegisterRequest) (*pb.RegisterResponse ,error){
	user,err:= u.registerReqToUserModel(request)
	if err != nil {
		u.logger.Errorf("registerReqToUserModel: %v", err)
		return nil, status.Errorf(grpc_errors.ParseGRPCErrStatusCode(err), "registerReqToUserModel: %v", err)
	}
	if err := utils.ValidateStruct(ctx, user); err != nil {
		u.logger.Errorf("ValidateStruct: %v", err)
		return nil, status.Errorf(grpc_errors.ParseGRPCErrStatusCode(err), "ValidateStruct: %v", err)
	}

	newUser,err:= u.userUseCase.Register(ctx,user)
	if err !=nil {
		u.logger.Errorf("userUseCase.Register: %v", err)
		return nil, status.Errorf(grpc_errors.ParseGRPCErrStatusCode(err), "userUseCase.Register: %v", err)
	}
	return &pb.RegisterResponse{
		User: u.userModelToProto(newUser.User),
		Token: newUser.Token,
	}, nil
}

func (u *userService) registerReqToUserModel(r *pb.RegisterRequest) (*models.User, error) {
	user := &models.User{
		Email:     r.GetEmail(),
		FirstName: r.GetFirstName(),
		LastName:  r.GetLastName(),
		Password:  r.GetPassword(),
	}
	return user, nil
}