package user

import (
	"fulfillment_auth/application/models"
	"fulfillment_auth/package/utils"
	"context"
)

type UserUseCase interface {
	Register(ctx context.Context, user *models.User) (*models.UserWithToken, error)
	Login(ctx context.Context, email string, password string) (*models.UserWithToken, error)
	Update(ctx context.Context, user *models.User) (*models.User, error)
	Delete(ctx context.Context, userID int) error
	GetByID(ctx context.Context, userID int) (*models.User, error)
	FindByName(ctx context.Context, name string) (*models.UsersList, error)
	GetUsers(ctx context.Context, pq *utils.PaginationQuery) (*models.UsersList, error)
}
