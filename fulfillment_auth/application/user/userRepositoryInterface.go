package user

import (
	"fulfillment_auth/application/models"
	"context"
	"fulfillment_auth/package/utils"
)

// User repository interface defined
type UserRepository interface {
	Create(ctx context.Context, user *models.User) (*models.User, error)
	Update(ctx context.Context, user *models.User) (*models.User, error)
	Delete(ctx context.Context, userID int) error
	GetByID(ctx context.Context, userID int) (*models.User, error)
	FindByName(ctx context.Context, name string) (*models.UsersList, error)
	FindByEmail(ctx context.Context, email string) (*models.User, error)
	GetUsers(ctx context.Context, pq *utils.PaginationQuery) (*models.UsersList, error)
}
