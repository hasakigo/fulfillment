package usecase

import (
	"context"
	"fmt"
	"fulfillment_auth/application/models"
	"fulfillment_auth/application/user"
	"fulfillment_auth/config"
	"fulfillment_auth/package/logger"
	"fulfillment_auth/package/utils"
)

const (
	basePrefix    = "api-auth:"
	cacheDuration = 3600
)

type userUseCase struct {
	cfg	*config.Config
	logger logger.ILogger
	userRepo  user.UserRepository
	redisRepo user.UserRedisRepository
}

// User UseCase Init
func InitAuthUseCase(cfg *config.Config, log logger.ILogger, userRepo user.UserRepository, redisRepo user.UserRedisRepository) user.UserUseCase {
	return &userUseCase{cfg: cfg, logger: log, userRepo: userRepo, redisRepo: redisRepo}
}



// Get user by Name
func (u *userUseCase) FindByName(ctx context.Context, name string) (*models.UsersList, error){
	return u.userRepo.FindByName(ctx, name)
	return nil, nil
}
// get list user
func (u *userUseCase) GetUsers(ctx context.Context, pq *utils.PaginationQuery) (*models.UsersList, error) {
	// Get users with pagination
	return u.userRepo.GetUsers(ctx, pq)
	return nil, nil
}
// generate key redis
func (u *userUseCase) GenerateUserKey(userID string) string {
	return fmt.Sprintf("%s: %s", basePrefix, userID)
}