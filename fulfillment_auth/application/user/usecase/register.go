package usecase

import (
	"context"
	"fulfillment_auth/application/models"
	"fulfillment_auth/package/utils"
	"github.com/pkg/errors"
)

// Create user
func (u *userUseCase) Register(ctx context.Context, user *models.User) (*models.UserWithToken, error) {
	existsUser, err := u.userRepo.FindByEmail(ctx, user.Email)
	if existsUser != nil || err == nil {
		return nil, errors.New("user.Register.Register duplicated user")
	}
	if err = user.PrepareCreate(); err != nil {
		return nil,errors.New("user.Register.PrepareCreate")
	}
	createdUser, err := u.userRepo.Create(ctx, user)
	if err != nil {
		return nil, err
	}
	createdUser.SanitizePassword()
	token, err := utils.GenerateJWTToken(createdUser, u.cfg)
	if err != nil {
		return nil, errors.New("user.Register.GenerateJWTToken error")
	}
	return &models.UserWithToken{
		User:  createdUser,
		Token: token,
	}, nil
	return nil, nil
}
