package usecase

import (
	"context"
	"strconv"
)

// delete user
func (u *userUseCase) Delete(ctx context.Context, userID int) error {
	if err := u.userRepo.Delete(ctx, userID); err != nil {
		return err
	}

	if err := u.redisRepo.DeleteUserCtx(ctx, u.GenerateUserKey(strconv.Itoa(userID))); err != nil {
		u.logger.Errorf("AuthUC.Delete.DeleteUserCtx: %s", err)
	}
	return nil
}