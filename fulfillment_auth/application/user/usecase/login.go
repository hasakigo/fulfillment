package usecase

import (
	"context"
	"fulfillment_auth/application/models"
	"fulfillment_auth/package/utils"
	"github.com/pkg/errors"
)

// Login
func (u *userUseCase) Login(ctx context.Context, email string, password string) (*models.UserWithToken, error){
	foundUser, err := u.userRepo.FindByEmail(ctx, email)
	if err != nil {
		return nil, err
	}
	if err = foundUser.ComparePasswords(password); err != nil {
		return nil,errors.New("user.GetUsers.ComparePasswords")
	}
	foundUser.SanitizePassword()
	token, err := utils.GenerateJWTToken(foundUser, u.cfg)
	if err != nil {
		return nil, errors.New("user.GetUsers.GenerateJWTToken")
	}
	return &models.UserWithToken{
		User:  foundUser,
		Token: token,
	}, nil
}