package usecase

import (
	"context"
	"strconv"
	"fulfillment_auth/application/models"
)
// Get user by ID
func (u *userUseCase) GetByID(ctx context.Context, userID int) (*models.User, error) {
	cachedUser, err := u.redisRepo.GetByIDCtx(ctx, u.GenerateUserKey(strconv.Itoa(userID)))
	if err != nil {
		u.logger.Errorf("authUC.GetByID.GetByIDCtx: %v", err)
	}
	if cachedUser != nil {
		return cachedUser, nil
	}

	user, err := u.userRepo.GetByID(ctx, userID)
	if err != nil {
		return nil, err
	}

	if err = u.redisRepo.SetUserCtx(ctx, u.GenerateUserKey(strconv.Itoa(userID)), cacheDuration, user); err != nil {
		u.logger.Errorf("authUC.GetByID.SetUserCtx: %v", err)
	}

	user.SanitizePassword()

	return user, nil
}