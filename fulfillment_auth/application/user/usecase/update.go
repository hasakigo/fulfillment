package usecase

import (
	"context"
	"fulfillment_auth/application/models"
	"strconv"
)
// update user
func (u *userUseCase) Update(ctx context.Context, user *models.User) (*models.User, error) {

	if err := user.PrepareUpdate(); err != nil {
		return nil, err
	}

	updatedUser, err := u.userRepo.Update(ctx, user)
	if err != nil {
		return nil, err
	}

	updatedUser.SanitizePassword()

	if err = u.redisRepo.DeleteUserCtx(ctx, u.GenerateUserKey(strconv.Itoa(user.ID))); err != nil {
		u.logger.Errorf("userUseCase.Update.DeleteUserCtx: %s", err)
	}

	updatedUser.SanitizePassword()

	return updatedUser, nil

	return nil, nil
}
