package repository

import (
	"fulfillment_auth/application/models"
	"fulfillment_auth/application/user"
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"time"
)

// Auth redis repository
type userRedisRepo struct {
	redisClient *redis.Client
}

// Auth redis repository constructor
func InitAuthRedisRepo(redisClient *redis.Client) user.UserRedisRepository {
	return &userRedisRepo{redisClient: redisClient}
}

// Get user by id
func (a *userRedisRepo) GetByIDCtx(ctx context.Context, key string) (*models.User, error) {
	userBytes, err := a.redisClient.Get(ctx, key).Bytes()
	if err != nil {
		return nil, err
	}
	user := &models.User{}
	if err = json.Unmarshal(userBytes, user); err != nil {
		return nil,err
	}
	return user, nil
}

// Cache user with duration in seconds
func (a *userRedisRepo) SetUserCtx(ctx context.Context, key string, seconds int, user *models.User) error {
	userBytes, err := json.Marshal(user)
	if err != nil {
		return err
	}
	if err = a.redisClient.Set(ctx, key, userBytes, time.Second*time.Duration(seconds)).Err(); err != nil {
		return err
	}
	return nil
}

// Delete user by key
func (a *userRedisRepo) DeleteUserCtx(ctx context.Context, key string) error {
	if err := a.redisClient.Del(ctx, key).Err(); err != nil {
		return err
	}
	return nil
}

func (a *userRedisRepo) getUserHashType(ctx context.Context, key string) (*models.User, error) {
	user := &models.User{}
	err := a.redisClient.HGetAll(ctx, key).Scan(&user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (a *userRedisRepo) setUserHashType(ctx context.Context, key string, seconds int, user *models.User) error {
	err:= a.redisClient.HSet(ctx,key,user,time.Second*time.Duration(seconds)).Err()
	if err != nil {
		return err
	}
	return nil
}