package repository
// Call DB to curd
import (
	"context"
	"fulfillment_auth/application/models"
	"fulfillment_auth/application/user"
	"fulfillment_auth/package/utils"
	"github.com/jinzhu/gorm"
)

type userRepository struct {
	db *gorm.DB
}

// Init User Repository

func InitUserRepository(db *gorm.DB) user.UserRepository {
	return &userRepository{db: db}
}
// Create new user
func (ur *userRepository) Create(ctx context.Context, user *models.User) (*models.User, error) {
	// call insert DB
	err:= ur.db.Create(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}
// Update user
func (ur *userRepository) Update(ctx context.Context, user *models.User) (*models.User, error) {
	u := &models.User{}
	// call insert DB
	return u, nil
}

// Delete user
func (ur *userRepository) Delete(ctx context.Context, userID int) error {
	// call insert DB
	return nil
}

// GetByID
func (ur *userRepository) GetByID(ctx context.Context, userID int) (*models.User, error){
	// call insert DB
	u := &models.User{}
	return u,nil
}

// Find user by email
func (ur *userRepository) FindByEmail(ctx context.Context, email string) (*models.User, error) {
	foundUser := &models.User{}
	if err := ur.db.Model(models.User{}).Where("email = ?",email).Take(&foundUser).Error;err!=nil{
		return nil, err
	}
	return foundUser, nil
}
// getListUser
func (ur *userRepository) GetUsers(ctx context.Context, pq *utils.PaginationQuery) (*models.UsersList, error) {
	listUser := &models.UsersList{}
	return listUser, nil
}

func (ur *userRepository) FindByName(ctx context.Context, name string) (*models.UsersList, error) {
	return nil, nil
}
