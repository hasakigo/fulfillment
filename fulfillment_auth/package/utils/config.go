package utils
// Get config path by app environment
func GetConfigPath(configPath string) string {
	if configPath == "qc" {
		return "./config/config-qc"
	}
	if configPath == "staging" {
		return "./config/config-staging"
	}
	if configPath == "prod" {
		return "./config/config-prod"
	}
	return "./config/config-local"
}

