package main

import (
	"fulfillment_auth/application/server"
	"fulfillment_auth/config"
	"fulfillment_auth/package/logger"
	"fulfillment_auth/package/redis"
	"fulfillment_auth/package/sql/mysql"
	"fulfillment_auth/package/utils"
	"log"
	"os"
)

func main()  {
	configPath := utils.GetConfigPath(os.Getenv("appEnv"))
	config,err := config.GetConfig(configPath)
	if err!=nil {
		log.Fatalf("Loading config error : %v",err)
	}
	// Init AppLog
	logger.Newlogger(logger.ConfigLogger{})
	appLogger := logger.GetLogger()
	redisClient := redis.InitConnection(config)
	defer redisClient.Close()
	mydb,err := mysql.InitConnection(config)
	if err !=nil {
		appLogger.Fatalf("Mysql Init error %v",err)
	}
	defer mydb.Close()
	authServer:= server.InitServer(appLogger, config, mydb,redisClient )
	appLogger.Fatal(authServer.Run())

}
