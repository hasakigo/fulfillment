# Document

* [GO](https://go.dev/doc/)
* [GRPC](https://grpc.io/)
* [Protocol-buffers](https://developers.google.com/protocol-buffers/docs/gotutorial)
### Run portobuf file
~~~
generateProto:
	protoc proto/**/*.proto --go_out=plugins=grpc:.
~~~

### Run application
~~~
server:
	go run cmd/main.go
~~~

### Run docker
~~~
docker build -t auth_svc .
docker run --name auth_svc_local -p 5001:5001 -d auth_svc
qc: docker run --name auth_svc_local -p 5001:5001 -e appEnv="qc" -d auth_svc
staging: docker run --name auth_svc_local -p 5001:5001 -e appEnv="staging" -d auth_svc
Prod: docker run --name auth_svc_local -p 5001:5001 -e appEnv="prod" -d auth_svc
~~~