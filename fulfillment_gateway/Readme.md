# Document

* [GO](https://go.dev/doc/)
* [GIN](https://gin-gonic.com/docs/introduction/)
* [GRPC](https://grpc.io/)
* [Protocol-buffers](https://developers.google.com/protocol-buffers/docs/gotutorial)
### Run portobuf file
~~~
generateProto:
	protoc proto/**/*.proto --go_out=plugins=grpc:.
~~~

### Run application
~~~
server:
	go run cmd/main.go
~~~

### Run docker
~~~
docker build -t gateway .
docker run --name gateway_local -p 3000:3000 -d gateway
qc: docker run --name gateway_qc -p 3000:3000 -e appEnv="qc" -d gateway
staging: docker run --name gateway_staging -p 3000:3000 -e appEnv="staging" -d gateway
Prod: docker run --name gateway_prod -p 3000:3000 -e appEnv="prod" -d gateway
~~~