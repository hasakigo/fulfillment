package routing

import(
    "fulfillment_gateway/application/model"
)

type ServiceClient interface {
    Invoke(routingData *model.RoutingData) (interface{}, error)
}