package routing

import (
    "fulfillment_gateway/application/model"
)

type RoutingUseCase interface {
	Forward(routingData *model.RoutingData) (interface{}, error)
}