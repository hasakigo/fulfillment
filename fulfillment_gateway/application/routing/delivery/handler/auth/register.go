package auth

import(
    "github.com/gin-gonic/gin"
    auth "fulfillment_gateway/proto/auth"
)

type registerUserHandler struct {
}

func InitRegisterUserHandler() *registerUserHandler {
    return &registerUserHandler{}
}

func (handler *registerUserHandler) Handle(ctx *gin.Context) (interface{}, error) {
    body := auth.RegisterRequest{}
    if err := ctx.BindJSON(&body); err != nil {
        return nil, err
    }

    return &body, nil
}
