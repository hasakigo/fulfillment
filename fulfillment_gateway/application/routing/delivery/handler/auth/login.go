package auth

import(
	"github.com/gin-gonic/gin"
	auth "fulfillment_gateway/proto/auth"
)

type loginUserHandler struct {
}

func InitLoginUserHandler() *loginUserHandler {
	return &loginUserHandler{}
}

func (handler *loginUserHandler) Handle(ctx *gin.Context) (interface{}, error) {
	body := auth.LoginRequest{}
	if err := ctx.BindJSON(&body); err != nil {
		return nil, err
	}

	return &body, nil
}
