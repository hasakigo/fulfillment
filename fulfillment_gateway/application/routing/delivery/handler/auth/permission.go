package auth

import(
	"github.com/gin-gonic/gin"
	perm "fulfillment_gateway/proto/auth/permission"
)

type createPermissionHandler struct {
}

func InitCreatePermissionHandler() *createPermissionHandler {
	return &createPermissionHandler{}
}

func (handler *createPermissionHandler) Handle(ctx *gin.Context) (interface{}, error) {
	body := perm.PermissionRequest{}
	if err := ctx.BindJSON(&body); err != nil {
		return nil, err
	}

	return &body, nil
}
