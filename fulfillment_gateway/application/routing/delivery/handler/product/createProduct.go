package handler

import(
    "github.com/gin-gonic/gin"
)

type createProductHandler struct {
}

func NewCreateProductHandler() *createProductHandler {
    return &createProductHandler{}
}

type CreateProductRequestBody struct {
	Name  string `json:"name"`
	Stock int64  `json:"stock"`
	Price int64  `json:"price"`
}

func (handler *createProductHandler) Handle(ctx *gin.Context) (interface{}, error) {
    body := CreateProductRequestBody{}
    if err := ctx.BindJSON(&body); err != nil {
        return nil, err
    }

    return &body, nil
}
