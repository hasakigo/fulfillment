package handler

import(
    "github.com/gin-gonic/gin"
    "strconv"
)

type findOneProductHandler struct {
}

func NewFindOneProductHandler() *findOneProductHandler {
    return &findOneProductHandler{}
}

type FindOneProductRequestParam struct {
	Id  int64 `json:"id"`
}

func (handler *findOneProductHandler) Handle(ctx *gin.Context) (interface{}, error) {
    id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
    if err != nil {
        return nil, err 
    }
    
    return &FindOneProductRequestParam {
        Id: id,
    }, nil
}
