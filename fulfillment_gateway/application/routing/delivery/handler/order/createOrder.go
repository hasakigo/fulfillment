package handler

import(
    "github.com/gin-gonic/gin"
)

type createOrderHandler struct {
}

func NewCreateOrderHandler() *createOrderHandler {
    return &createOrderHandler{}
}

type CreateOrderRequestBody struct {
	ProductId int64 `json:"productId"`
    Quantity  int64 `json:"quantity"`
}

func (handler *createOrderHandler) Handle(ctx *gin.Context) (interface{}, error) {
    body := CreateOrderRequestBody{}
    if err := ctx.BindJSON(&body); err != nil {
        return nil, err
    }

    return &body, nil
}
