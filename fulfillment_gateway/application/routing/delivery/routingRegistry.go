package delivery

import(
    "fulfillment_gateway/application/routing/delivery/handler"
    auth "fulfillment_gateway/application/routing/delivery/handler/auth"
    product "fulfillment_gateway/application/routing/delivery/handler/product"
    order "fulfillment_gateway/application/routing/delivery/handler/order"
)

type routingConfig struct {
    requestHandler handler.RequestHandler
    remoteServiceName string
    remoteServiceMethod string
}

var routingRegistry = map[string]routingConfig {
    "GET:/product/:id": routingConfig {
        product.NewFindOneProductHandler(),
        "product",
        "findOne",
    },

    "POST:/product/create": routingConfig {
        product.NewCreateProductHandler(),
        "product",
        "create",
    },
    "POST:/order/create": routingConfig {
        order.NewCreateOrderHandler(),
        "order",
        "create",
    },
    "POST:/api/auth/register": routingConfig {
        auth.InitRegisterUserHandler(),
        "auth",
        "register",
    },
    "POST:/api/login": routingConfig {
        auth.InitLoginUserHandler(),
        "auth",
        "login",
    },
    "POST:/api/auth/permission": routingConfig {
        auth.InitCreatePermissionHandler(),
        "auth",
        "permission",
    },
}