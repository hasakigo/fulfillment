package service

import(
    "fulfillment_gateway/config"
    "fulfillment_gateway/application/routing/delivery/remote"
    "fulfillment_gateway/application/routing/delivery/remote/grpc"
)

func initRemoteServiceClientRegistry(config *config.Config) map[string]remote.RemoteServiceClient {
    return map[string]remote.RemoteServiceClient {
        "product": remote.NewGrpcServiceClient(grpc.NewProductClient(config)),
        "order": remote.NewGrpcServiceClient(grpc.NewOrderClient(config)),
        "auth": remote.NewGrpcServiceClient(grpc.NewAuthClient(config)),
        "permission": remote.NewGrpcServiceClient(grpc.NewPermissionClient(config)),
    }
}
