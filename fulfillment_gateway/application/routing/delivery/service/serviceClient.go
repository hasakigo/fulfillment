package service

import(
    "errors"
    "fulfillment_gateway/config"
    "fulfillment_gateway/application/model"
    "fulfillment_gateway/application/routing/delivery/remote"
)

type serviceClient struct {
    remoteServiceClientRegistry map[string]remote.RemoteServiceClient
}

func NewServiceClient(config *config.Config) *serviceClient {
    return &serviceClient{
        initRemoteServiceClientRegistry(config),
    }
}

func (service *serviceClient) Invoke(routingData *model.RoutingData) (interface{}, error) {
    remoteServiceClient, found := service.remoteServiceClientRegistry[routingData.ServiceName]
    if (!found) {
        return nil, errors.New("remote service client not found: " + routingData.ServiceName)
    }

    return remoteServiceClient.Invoke(routingData.ServiceMethod, routingData.Payload)
}
