package remote

import(
    "errors"
    "reflect"
    "bytes"
    "encoding/gob"
)

type defaultGrpcServiceClient struct {
    grpcClient GrpcServiceClient
}

func NewGrpcServiceClient(grpcClient GrpcServiceClient) *defaultGrpcServiceClient {
    return &defaultGrpcServiceClient {
        grpcClient: grpcClient,
    }
}

func (defaultClient defaultGrpcServiceClient) Invoke(method string, data interface{}) (interface{}, error) {
    methodCfg, found := defaultClient.grpcClient.GetMethodRegistry()[method]
    if !found {
        return nil, errors.New("Method not found: " + method)
    }

    grpcData, err := convertToGrpcType(data, methodCfg.GrpcDataType)
    if err != nil {
        return nil, err
    }

    return methodCfg.Invoke(grpcData)
}

func convertToGrpcType(in interface{}, outType reflect.Type) (interface{}, error) {
    var buffer bytes.Buffer
    enc := gob.NewEncoder(&buffer) // will write to buffer
    dec := gob.NewDecoder(&buffer) // will read from buffer

    err := enc.Encode(in)
    if err != nil {
        return nil, err
    }

    out := reflect.New(outType).Elem().Addr().Interface()
    err = dec.Decode(out)
    if err != nil {
        return nil, err
    }

    return out, nil
}