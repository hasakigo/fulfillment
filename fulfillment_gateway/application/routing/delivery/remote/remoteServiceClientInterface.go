package remote

type RemoteServiceClient interface {
    Invoke(method string, data interface{}) (interface{}, error)
}