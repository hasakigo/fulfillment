package remote

import(
    "reflect"
)

type MethodConfig struct {
    GrpcDataType reflect.Type
    Invoke func(interface{}) (interface{}, error)
}

type GrpcServiceClient interface {
    GetMethodRegistry() map[string]MethodConfig
}
