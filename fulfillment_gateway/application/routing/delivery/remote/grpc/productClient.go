package grpc

import(
    "google.golang.org/grpc"
    "context"
    "reflect"
    "fmt"

    "fulfillment_gateway/config"
    "fulfillment_gateway/application/routing/delivery/remote"
    "fulfillment_gateway/proto/product"
)

func (client *productClient) initMethodRegistry() {
    client.methodRegistry = map[string]remote.MethodConfig {
        "findOne": remote.MethodConfig {
            reflect.TypeOf(product.FindOneRequest{}),
            client.findOneProduct,
        },
        "create": remote.MethodConfig {
            reflect.TypeOf(product.CreateProductRequest{}),
            client.createProduct,
        },
    }
}

type productClient struct {
    grpcClient product.ProductServiceClient
    methodRegistry map[string]remote.MethodConfig
}

func NewProductClient(config *config.Config) *productClient {
    // using WithInsecure() because no SSL running
    cc, err := grpc.Dial(config.Service.PRODUCT_SVC_URL, grpc.WithInsecure())

    if err != nil {
        fmt.Println("Could not connect:", err)
    }

    client := productClient{
        grpcClient: product.NewProductServiceClient(cc),
    }
    client.initMethodRegistry()

    return &client
}

func (client *productClient) GetMethodRegistry() map[string]remote.MethodConfig {
    return client.methodRegistry
}

func (client *productClient) findOneProduct(data interface{}) (interface{}, error) {
    return client.grpcClient.FindOne(context.Background(), data.(*product.FindOneRequest))
}

func (client *productClient) createProduct(data interface{}) (interface{}, error) {
    return client.grpcClient.CreateProduct(context.Background(), data.(*product.CreateProductRequest))
}