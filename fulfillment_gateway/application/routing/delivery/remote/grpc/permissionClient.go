package grpc

import(
    "google.golang.org/grpc"
    "context"
    "reflect"
    "fmt"

    "fulfillment_gateway/config"
    "fulfillment_gateway/application/routing/delivery/remote"
    perm "fulfillment_gateway/proto/auth/permission"
)

func (client *permissionClient) initMethodRegistry() {
    client.methodRegistry = map[string]remote.MethodConfig {
        "create": remote.MethodConfig {
            reflect.TypeOf(perm.PermissionRequest{}),
            client.createPermission,
        },
    }
}

type permissionClient struct {
    permGrpcClient perm.AuthServiceClient
    methodRegistry map[string]remote.MethodConfig
}

func NewPermissionClient(config *config.Config) *permissionClient {
    // using WithInsecure() because no SSL running
    cc, err := grpc.Dial(config.Service.AUTH_SVC_URL, grpc.WithInsecure())

    if err != nil {
        fmt.Println("Could not connect:", err)
    }

    client := permissionClient{
        permGrpcClient: perm.NewAuthServiceClient(cc),
    }
    client.initMethodRegistry()

    return &client
}

func (client *permissionClient) GetMethodRegistry() map[string]remote.MethodConfig {
    return client.methodRegistry
}

func (client *permissionClient) createPermission(data interface{}) (interface{}, error) {
    return client.permGrpcClient.Create(context.Background(), data.(*perm.PermissionRequest))
}