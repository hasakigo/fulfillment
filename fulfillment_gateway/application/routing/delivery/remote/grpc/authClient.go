package grpc

import(
    "google.golang.org/grpc"
    "context"
    "reflect"
    "fmt"

    "fulfillment_gateway/config"
    "fulfillment_gateway/application/routing/delivery/remote"
    auth "fulfillment_gateway/proto/auth"
)

func (client *authClient) initMethodRegistry() {
    client.methodRegistry = map[string]remote.MethodConfig {
        "register": remote.MethodConfig {
            reflect.TypeOf(auth.RegisterRequest{}),
            client.registerUser,
        },
        "login": remote.MethodConfig {
            reflect.TypeOf(auth.LoginRequest{}),
            client.login,
        },
    }
}

type authClient struct {
    authGrpcClient auth.AuthServiceClient
    methodRegistry map[string]remote.MethodConfig
}

func NewAuthClient(config *config.Config) *authClient {
    // using WithInsecure() because no SSL running
    cc, err := grpc.Dial(config.Service.AUTH_SVC_URL, grpc.WithInsecure())

    if err != nil {
        fmt.Println("Could not connect:", err)
    }

    client := authClient{
        authGrpcClient: auth.NewAuthServiceClient(cc),
    }
    client.initMethodRegistry()

    return &client
}

func (client *authClient) GetMethodRegistry() map[string]remote.MethodConfig {
    return client.methodRegistry
}

func (client *authClient) registerUser(data interface{}) (interface{}, error) {
    return client.authGrpcClient.Register(context.Background(), data.(*auth.RegisterRequest))
}

func (client *authClient) login(data interface{}) (interface{}, error) {
    return client.authGrpcClient.Login(context.Background(), data.(*auth.LoginRequest))
}
