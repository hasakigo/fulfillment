package grpc

import(
    "google.golang.org/grpc"
    "context"
    "reflect"
    "fmt"

    "fulfillment_gateway/config"
    "fulfillment_gateway/application/routing/delivery/remote"
    "fulfillment_gateway/proto/order"
)

func (client *orderClient) initMethodRegistry() {
    client.methodRegistry = map[string]remote.MethodConfig {
        "create": remote.MethodConfig {
            reflect.TypeOf(order.CreateOrderRequest{}),
            client.createOrder,
        },
    }
}

type orderClient struct {
    grpcClient order.OrderServiceClient
    methodRegistry map[string]remote.MethodConfig
}

func NewOrderClient(config *config.Config) *orderClient {
    // using WithInsecure() because no SSL running
    cc, err := grpc.Dial(config.Service.ORDER_SVC_URL, grpc.WithInsecure())

    if err != nil {
        fmt.Println("Could not connect:", err)
    }

    client := orderClient{
        grpcClient: order.NewOrderServiceClient(cc),
    }
    client.initMethodRegistry()

    return &client
}

func (client *orderClient) GetMethodRegistry() map[string]remote.MethodConfig {
    return client.methodRegistry
}

func (client *orderClient) createOrder(data interface{}) (interface{}, error) {
    return client.grpcClient.CreateOrder(context.Background(), data.(*order.CreateOrderRequest))
}