package delivery

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"

	"fulfillment_gateway/application/routing"
	"fulfillment_gateway/config"
	"fulfillment_gateway/application/model"
	"fulfillment_gateway/application/routing/delivery/authz"
)

type routingHandler struct {
    routingUC routing.RoutingUseCase
    permissionService authz.PermissionService
}

func RegisterRoutes(r *gin.Engine, c *config.Config, routingUC routing.RoutingUseCase) {
    handler := &routingHandler{
        routingUC: routingUC,
        permissionService: authz.NewPermissionService(c),
    }
	routes := r.Group("/api")
	routes.POST("/login", handler.handle)

	/* TODO: all api Products defined here */
	routes = r.Group("/api/product")
	routes.GET("/:id", handler.handle)
	routes.POST("/create", handler.handle)

	/* TODO: all api Order defined here */
	routes = r.Group("/api/order")
	routes.GET("/create", handler.handle)
	routes.PUT("/update", handler.handle)
	routes.POST("/create", handler.handle)

	/* TODO: all api Auth defined here */
	routes = r.Group("/api/auth")
    routes.POST("/register", handler.handle)
	routes.POST("/permission", handler.handle)
}

func (handler *routingHandler) handle(ctx *gin.Context) {
    if (!handler.permissionService.Authorize(ctx)) {
        return
    }
    route := ctx.Request.Method + ":" + ctx.FullPath()
    routingConfig, found :=  routingRegistry[route];
    if !found {
        fmt.Println("No routing config found for route:", route)
        ctx.AbortWithStatus(http.StatusInternalServerError)
        return
    }

    data, err := routingConfig.requestHandler.Handle(ctx)
    if err != nil {
        fmt.Println("Failed to handler incoming request:", route, err)
        ctx.AbortWithError(http.StatusBadRequest, err)
        return
    }

	routingData := &model.RoutingData {
	    ServiceName: routingConfig.remoteServiceName,
	    ServiceMethod: routingConfig.remoteServiceMethod,
	    Payload: data,
	}

	res, err := handler.routingUC.Forward(routingData)
	if (err != nil) {
	    ctx.AbortWithError(http.StatusInternalServerError, err)
	    return
	}

	ctx.JSON(http.StatusOK, res)
}
