package authz

import (
    "github.com/gin-gonic/gin"
    "net/http"
    "strings"

    "fulfillment_gateway/config"
)

type permissionService struct {
    config *config.Config
}

func NewPermissionService(config *config.Config) PermissionService {
    return &permissionService {
        config,
    }
}

func (service *permissionService) Authorize(ctx *gin.Context) bool {
    authzHeader := ctx.Request.Header["Authorization"]

    if (len(authzHeader) == 0 || len(authzHeader[0]) < 20) {
        ctx.JSON(http.StatusUnauthorized, "")
        return false
    }

    jwt := strings.TrimSpace(authzHeader[0][20:len(authzHeader[0])])
    if (len(jwt) == 0) {
        ctx.JSON(http.StatusUnauthorized, "")
        return false
    }
    // TODO: extract principal (user name) from jwt
    // TODO: fetch user permission from redis & check if user can perform this request
    // TODO: if user have permission create_po_stock_1, approve_po_stock2, but request approve_stock_3?
    //
    return true
}