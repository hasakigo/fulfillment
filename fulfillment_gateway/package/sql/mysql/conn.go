package mysql

import (
	"fulfillment_gateway/config"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
)

const (
	maxOpenConns    = 60
	connMaxLifetime = 120
	maxIdleConns    = 30
	connMaxIdleTime = 20
)

func InitConnection(cfg *config.Config) (*gorm.DB, error)  {
	sqlConnection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", cfg.Mysql.MySqlUser, cfg.Mysql.MySqlPassword,cfg.Mysql.MySqlHost, cfg.Mysql.MySqlPort,cfg.Mysql.MySqlDbname)
	db, err := gorm.Open(cfg.Mysql.MysqlDriver, sqlConnection)
	if err != nil {
		return nil, err
	}

	db.DB().SetMaxOpenConns(maxOpenConns)
	db.DB().SetConnMaxLifetime(connMaxLifetime * time.Second)
	db.DB().SetMaxIdleConns(maxIdleConns)
	db.DB().SetConnMaxIdleTime(connMaxIdleTime * time.Second)
	if err = db.DB().Ping(); err != nil {
		return nil, err
	}
	return db,nil
}