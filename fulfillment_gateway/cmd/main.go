package main

import (
	"log"
	"fulfillment_gateway/config"
	"os"
	"fulfillment_gateway/package/utils"
	"fulfillment_gateway/package/logger"
	"fulfillment_gateway/package/redis"
	"github.com/gin-gonic/gin"
	"fulfillment_gateway/application/routing/delivery"
	"fulfillment_gateway/application/routing/delivery/service"
	"fulfillment_gateway/application/routing/usecase"

)

func main() {
	configPath := utils.GetConfigPath(os.Getenv("appEnv"))
	config,err := config.GetConfig(configPath)
	if err!=nil {
		log.Fatalf("Loading config error : %v",err)
	}
	// Init AppLog
	logger.Newlogger(logger.ConfigLogger{})
	appLogger := logger.GetLogger()
	redisClient := redis.InitConnection(config)
	appLogger.Info("redisClient Init success %v",redisClient)
	r := gin.Default()
    serviceClient := service.NewServiceClient(config)

	routingUC := usecase.NewRoutingUseCase(serviceClient)
	delivery.RegisterRoutes(r, config, routingUC)
	r.Run(config.Server.Port)
}
